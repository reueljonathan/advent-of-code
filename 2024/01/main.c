#include <stdlib.h>
#include <stdio.h>

#define INPUT_SIZE 1000


long long int llmax(long long a, long long b)
{
    return a > b ? a : b;
}

long long int llmin(long long a, long long b)
{
    return a < b ? a : b;
}

int compare(const void * pa, const void * pb)
{

    long long int a = *((long long int *)pa);
    long long int b = *((long long int *)pb);

    //printf("\ta: %lld, b: %lld, a - b: %lld\n", a, b, a-b);

    return a - b;
}

int main(void)
{
    FILE *input_file = fopen("input-01", "r");
    //FILE *input_file = fopen("example", "r");
    long long int left_column[INPUT_SIZE], right_column[INPUT_SIZE];
    long long int left, right;
    long long int result = 0;
    long long int similarity = 0;
    size_t index = 0;
    char line[16];

    if (input_file == NULL)
    {
        fprintf(stderr, "Could not open file.\n");
        exit(EXIT_FAILURE);
    }

    while (fgets(line, 16, input_file))
    {
        sscanf(line, "%lld   %lld\n", &left, &right);

        left_column[index] = left;
        right_column[index] = right;
        index++;
    }

    qsort(left_column, INPUT_SIZE, sizeof(long long int), compare);
    qsort(right_column, INPUT_SIZE, sizeof(long long int), compare);

    for (int i = 0; i < INPUT_SIZE; i++)
    {
        result += llmax(right_column[i], left_column[i]) - llmin(left_column[i], right_column[i]);
    }

    // print arrays partially
    /*
    for (int i = 0; i < INPUT_SIZE; i++)
    {
        printf("%lld %lld\n", left_column[i], right_column[i]);
    }
    */
    /*
    for (int i = 0; i < 20; i++)
    {
        printf("%lld %lld\n", left_column[i], right_column[i]);
    }
    printf("...\n");
    for (int i = 989; i < 1000; i++)
    {
        printf("%lld %lld\n", left_column[i], right_column[i]);
    }
    */

    // Solution part 2

    /* as the arrays are already sorted from solution 01, we can
     * just do a binary search in the second array, and if we find
     * the element, we count how many equal elements we have before and
     * after the found element, and multiply by itself.
     */

    long long int element;
    int start, end, middle;
    for (int i = 0; i < INPUT_SIZE; i++)
    {
        element = left_column[i]; 

        start = 0;
        end = INPUT_SIZE - 1;
        middle = (start + end) / 2;

        while (start < end && element != right_column[middle])
        {
            if (right_column[middle] < element)
            {
                start = middle + 1;
            }
            else if (right_column[middle] > element)
            {
                end = middle - 1;
            }
            else // found it
            {
                break;
            }

            middle = (start + end) / 2;
        }

        if (element == right_column[middle])
        {
            start = middle - 1;
            end = middle + 1;

            while (start >= 0 && right_column[start] == element)
            {
                start--;
            }

            while (end < INPUT_SIZE && right_column[end] == element)
            {
                end++;
            }

            printf("\tsimilatiry of %lld. start: %d, end: %d\n",
                    element, start, end);

            similarity += (end - (start + 1)) * element;
        }

    }

    printf("Result 01: %lld\n", result);
    printf("Result 02: %lld\n", similarity);

    fclose(input_file);
    return 0;
}
