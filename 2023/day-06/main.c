#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LINE_SIZE 1024

struct races {
    size_t num_races;
    unsigned long long int *times, *distance_records; 
};

struct races create_race(int max_num_races) {
    struct races result;

    result.num_races = 0;
    result.times = malloc(sizeof(unsigned long long int) * max_num_races);
    result.distance_records = malloc(sizeof(unsigned long long int) * max_num_races);

    return result;
}

void destroy_races(struct races races) {
    free(races.times);
    free(races.distance_records);
}

void read_line(char * line, unsigned long long * arr, size_t max_size, size_t * tokens_read) {
    size_t numbers_read = 0;
    char * token;
    strtok(line, " "); // discard first part

    while ((token = strtok(NULL, " ")) && numbers_read < max_size) {
        arr[numbers_read] = strtoull(token, NULL, 10);
        printf("token: %s, number: %llu\n", token, arr[numbers_read]);
        numbers_read++;
    }

    *tokens_read = numbers_read;
}

int main(int argc, char **argv) {
    FILE *input;
    char line[MAX_LINE_SIZE + 1];
    struct races races;
    unsigned long long result = 0;

    if (argc < 2) {
        fprintf(stderr, "No input file informed\n");
        return 1;
    }

    input = fopen(argv[1], "r");

    if (input == NULL) {
        fprintf(stderr, "Error when trying to open file '%s'\n", argv[1]);
        return 1;
    }

    races = create_race(5);

    // read times line
    fgets(line, MAX_LINE_SIZE + 1, input);
    read_line(line, races.times, 5, &races.num_races);

    // read records line
    fgets(line, MAX_LINE_SIZE + 1, input);
    read_line(line, races.distance_records, 5, &races.num_races);

    printf("          Time (ms)  Record distance (mm)\n");
    for (size_t i = 0; i < races.num_races; i++) {
        unsigned long long int num_winnings = 0;
        printf("Race #%2d: %9llu  %20llu\n", i + 1, races.times[i], races.distance_records[i]);

        // naive solution
        for (unsigned long long t = 0, total_time = races.times[i]; t <= total_time; t++) {
            if (t * (total_time - t) > races.distance_records[i]) {
                num_winnings++;
            }
        }

        //printf("Num winnings in the %2dnd race: %2d\n", i + 1, num_winnings);

        if (result == 0) result = num_winnings;
        else result *= num_winnings;
    }


    printf("Result: %llu\n", result);

    destroy_races(races);
    fclose(input);

    return 0;
}
