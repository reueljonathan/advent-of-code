#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

/* response 1: 2685
 * response 2: 83707
 */

#define MAX_LINE_SIZE 1024

struct str_split {
    size_t count;
    size_t capacity;
    char ** items;
    char delimiter;
};

void destroy_split(const struct str_split split) {
    if (split.count == 0) return;

    for (size_t i = 0; i < split.count; i++) {
        free(split.items[i]);
    }
    free(split.items);
}

struct str_split split(const char *str, const char delimiter) {
    struct str_split result;
    size_t str_len = 0, tkn_len;
    char *token, *str_copy;
    char delimiter_str[2] = { delimiter, '\0' };

    result.delimiter = delimiter;
    result.count     = 0;
    result.capacity  = 16;
    result.items     = NULL;

    if (str == NULL) return result;

    result.items = malloc(sizeof(char*) * result.capacity);
    assert(result.items != NULL);

    str_len = strlen(str);
    str_copy = malloc(sizeof(char) * (str_len + 1));

    assert(str_copy != NULL);
    strncpy(str_copy, str, str_len + 1);

    token = strtok(str_copy, delimiter_str);
    while (token) {
        tkn_len = strlen(token);

        result.items[result.count] = malloc(sizeof(char) * (tkn_len + 1));
        strncpy(result.items[result.count], token, tkn_len + 1);
        result.count++; 

        token = strtok(NULL, delimiter_str);
    }


    free(str_copy);
    return result;
}

// ### END split() implementation. TODO pass this to its own file. I'll probably
// use this a lot.

struct round {
    int red, green, blue;
    struct round *next;
};

struct game {
    int id;
    struct round * first_round;
    struct round * last_round;
    struct game *next;

    int min_red, min_green, min_blue;
};

struct game_list {
    struct game * first;
    struct game * last;
};

struct game * create_game(int id) {
    struct game * result = malloc(sizeof(struct game));

    assert(result != NULL);

    result->id = id;
    result->first_round = NULL;
    result->last_round = NULL;
    result->next = NULL;

    result->min_red   = 0;
    result->min_green = 0;
    result->min_blue  = 0;

    return result;
}

void insert_game_round(struct game * game, int red, int green, int blue) {
    assert(game != NULL);

    struct round * new_round = malloc(sizeof(struct round));
    assert(new_round != NULL);

    new_round->next  = NULL;
    new_round->red   = red;
    new_round->green = green;
    new_round->blue  = blue;

    if (!game->last_round) {
        game->first_round = new_round;
        game->last_round  = new_round;
    } else {
        game->last_round->next = new_round;
        game->last_round = new_round;
    }

    if (game->min_red == 0 || game->min_red < red) {
        game->min_red = red;
    }

    if (game->min_green == 0 || game->min_green < green) {
        game->min_green = green;
    }

    if (game->min_blue == 0 || game->min_blue < blue) {
        game->min_blue = blue;
    }
}

void destroy_game(struct game *game) {
    struct round *round, *aux;

    if (!game) return;

    round = game->first_round;

    while (round) {
        aux = round; 
        round = round->next;
        free(aux);
    }

    free(game);
}

void insert_game(struct game_list *game_list, struct game * game) {
    if (game == NULL) return;

    if (!game_list->last) {
        game_list->first = game;
        game_list->last  = game;
    } else {
        game_list->last->next = game;
        game_list->last = game_list->last->next;
    }
}

void destroy_game_list(struct game_list game_list) {
    struct game * game = game_list.first;
    struct game * aux;

    while (game != NULL) {
        aux = game;
        game = game->next;
        destroy_game(aux);
    }
}

void debug_print_games(struct game_list list) {
    struct game * game = list.first;
    struct round * round;

    while (game) {
        printf("Game %2d:\n", game->id);
        round = game->first_round;

        while (round) {
            printf("\tred: %2d, green: %2d, blue: %2d\n", round->red, round->green, round->blue); 
            round = round->next;
        }

        game = game->next;
    }
}

int check_valid_game(struct game * game, int total_red, int total_green, int total_blue) {
    if (game == NULL) return 0;

    struct round * round = game->first_round;

    while (round) {
        if (round->red > total_red || round->green > total_green || round->blue > total_blue)
            return 0;
        
        round = round->next;
    }

    return 1;
}

int calculate_sum_possible_games(struct game_list game_list, int red_balls, int green_balls, int blue_balls) {
    int result = 0;
    struct game * aux = game_list.first;

    while (aux) {

        printf("checking game %d\n", aux->id);
        
        if (check_valid_game(aux, red_balls, green_balls, blue_balls))
            result += aux->id;
        else
            printf("game %d invalid.\n", aux->id);

        aux = aux->next;
    }

    return result;
}

int calculate_power_sum(struct game_list game_list) {
    int result = 0;

    struct game * game = game_list.first;

    while (game) {

        printf("game %d -> mr: %d, mg: %d, mb: %d\n", game->id, game->min_red, game->min_green, game->min_blue);
        result += game->min_red * game->min_green * game->min_blue;
        printf("partial result: %d\n", result);

        game = game->next;
    }

    return result;
}

int main() {
    FILE * input = fopen("input", "r");
    char line[MAX_LINE_SIZE + 1], round[MAX_LINE_SIZE + 1];
    char * token, *round_token, *round_colors_token;
    int game_id;
    struct game * game;
    struct game_list game_list;
    game_list.first = NULL;
    game_list.last = NULL;

    if (input == NULL) {
        fprintf(stderr, "Could not open input file\n");
        return 1;
    }

    while (fgets(line, MAX_LINE_SIZE + 1, input) && !feof(input)) {
        struct str_split game_split = split(line, ':');

        if (game_split.count > 0 && strncmp("Game ", game_split.items[0], 5) == 0) {
            game_id = atoi(game_split.items[0] + 5); 

            game = create_game(game_id);

            struct str_split rounds_split = split(game_split.items[1], ';');

            for (size_t i = 0; i < rounds_split.count; i++) {
                struct str_split colors_split = split(rounds_split.items[i], ',');
                struct round round;

                round.red = 0;
                round.green = 0;
                round.blue = 0;

                for (size_t j = 0; j < colors_split.count; j++) {
                    int quantity;

                    token = strtok(colors_split.items[j], " ");
                    quantity = atoi(token);
                    token = strtok(NULL, " ");

                    if (strncmp(token, "red", 3) == 0) {
                        round.red = quantity;
                    } else if (strncmp(token, "green", 5) == 0) {
                        round.green = quantity;
                    } else if (strncmp(token, "blue", 4) == 0) {
                        round.blue = quantity;
                    }

                }

                insert_game_round(game, round.red, round.green, round.blue);
                destroy_split(colors_split);
            }

            insert_game(&game_list, game);
            destroy_split(rounds_split);
        }
        destroy_split(game_split);
    }

    //debug_print_games(game_list);

    printf("sum valid games ids: %d\n", calculate_sum_possible_games(game_list, 12, 13, 14));
    printf("sum game powers: %d\n", calculate_power_sum(game_list));

    destroy_game_list(game_list);
    fclose(input);

    /*
    struct game_list game_list;
    game_list.first = NULL;
    game_list.last = NULL;

    struct game * game_1 = create_game(1);
    //                        r  g  b
    insert_game_round(game_1, 4, 0, 3);
    insert_game_round(game_1, 1, 2, 6);
    insert_game_round(game_1, 0, 2, 0);

    struct game * game_2 = create_game(2);

    insert_game_round(game_2, 0, 2, 1);
    insert_game_round(game_2, 1, 3, 4);
    insert_game_round(game_2, 0, 1, 1);

    insert_game(&game_list, game_1);
    insert_game(&game_list, game_2);

    debug_print_games(game_list);

    destroy_game_list(game_list);
    */

    return 0;
}
