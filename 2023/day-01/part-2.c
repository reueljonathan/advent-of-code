#include <stdio.h>
#include <string.h>

#define MAX_LINE_SIZE 128

int cmp_str_with_numbers(char * s) {
    switch (*s) {
        case 'o':
        {
            if (strncmp(s, "one", 3) == 0) {
                return 1;
            }
            break;
        }
        case 't':
        {
            if (strncmp(s, "two", 3) == 0) {
                return 2;
            } else if (strncmp(s, "three", 5) == 0) {
                return 3;
            }
            break;
        }
        case 'f':
        {
            if (strncmp(s, "four", 4) == 0) {
                return 4;
            } else if (strncmp(s, "five", 4) == 0) {
                return 5;
            }
            break;
        }
        case 's':
        {
            if (strncmp(s, "six", 3) == 0) {
                return 6;
            } else if (strncmp(s, "seven", 5) == 0) {
                return 7;
            }
            break;
        }
        case 'e':
        {
            if (strncmp(s, "eight", 5) == 0) {
                return 8;
            }
            break;
        }
        case 'n':
        {
            if (strncmp(s, "nine", 4) == 0) {
                return 9;
            }
            break;
        }
        default: {
            if (*s >= '0' && *s <= '9') {
                return (*s) - '0';
            }

            break;
        }
    }

    return 0;
}

int find_first_digit(char * s) {
    int digit = 0;

    while (*s) {
        digit = cmp_str_with_numbers(s);

        if (digit) return digit;

        s++;
    }

    return 0;
}

int find_last_digit(char * s, size_t len) {
    int digit = 0;

    for (size_t i = len - 1; i >= 0; i--) {
        digit = cmp_str_with_numbers(s + i);

        if (digit) return digit;
    }

    return 0;
}

int main() {
    FILE * input = fopen("input", "r");
    char line[MAX_LINE_SIZE + 1];
    size_t current_line_len;
    long long int calibration, sum = 0;

    if (file == NULL) {
        fprintf(stderr, "Error when opening file");
    }

    while (fgets(line, MAX_LINE_SIZE + 1, input) && !feof(input)) {
        current_line_len = strlen(line);
        sum += find_first_digit(line) * 10 + find_last_digit(line, current_line_len);
    }

    printf("sum: %lld\n", sum);

    fclose(input);
    return 0;
}
