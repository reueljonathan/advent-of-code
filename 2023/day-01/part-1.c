#include <stdio.h>
#include <string.h>

#define MAX_LINE_SIZE 128

int find_first_digit(char * s) {
    while (s) {
        if (*s >= '0' && *s <= '9') return (*s) - '0';
        s++;
    }
}

int find_last_digit(char * s, size_t len) {
    for (size_t i = len - 1; i >= 0; i--) {
        if (s[i] >= '0' && s[i] <= '9') return s[i] - '0';
    }
}

int main() {
    FILE * input = fopen("input", "r");
    char line[MAX_LINE_SIZE + 1];
    size_t current_line_len;
    long long int calibration, sum = 0;

    while (fgets(line, MAX_LINE_SIZE + 1, input) && !feof(input)) {
        current_line_len = strlen(line);
        calibration = find_first_digit(line) * 10 + find_last_digit(line, current_line_len);
        sum += calibration;
    }

    printf("sum: %lld\n", sum);

    fclose(input);
    return 0;
}
