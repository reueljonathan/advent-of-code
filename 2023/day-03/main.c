#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define MAX_LINE_SIZE 1024

struct symbol {
    int             row;
    int             column;
    char            symbol;
    struct symbol * next;
};

struct symbol_list {
    struct symbol * first;
    struct symbol * last;
};

struct number {
    int             number;
    int             row;
    int             column;
    int             width;
    struct number * next;
};

struct number_list {
    struct number * first;
    struct number * last;
};

// #### SYMBOL LINKED LIST
struct symbol_list * create_symbol_list() {
    struct symbol_list * result = malloc(sizeof(struct symbol_list));

    assert(result);

    result->first = NULL;
    result->last = NULL;

    return result;
}

struct symbol * create_symbol(char symbol, int row, int column) {
    struct symbol * result = malloc(sizeof(struct symbol));

    assert(result);

    result->symbol = symbol;
    result->row    = row;
    result->column = column;
    result->next   = NULL;

    return result;
}

void insert_symbol(struct symbol_list * list, struct symbol * symbol) {
    assert(list);

    if (list->first == NULL) {
        list->first = list->last = symbol;
    } else {
        list->last->next = symbol;
        list->last = symbol;
    }
}

void destroy_symbol_list(struct symbol_list * list) {
    struct symbol * aux;
    if (list == NULL) return;
    
    while (list->first) {
        aux = list->first; 
        list->first = list->first->next;
        free(aux);
    }

    free(list);
}

// ### NUMBER LINKED LIST
struct number_list * create_number_list() {
    struct number_list * result = malloc(sizeof(struct number_list));

    assert(result);

    result->first = NULL;
    result->last = NULL;

    return result;
}

struct number * create_number(int number, int row, int column, int width) {
    struct number * result = malloc(sizeof(struct number));

    assert(result);

    result->number = number;
    result->row    = row;
    result->column = column;
    result->width  = width;
    result->next   = NULL;

    return result;
}

void insert_number(struct number_list * list, struct number * number) {
    assert(list);

    if (list->first == NULL) {
        list->first = list->last = number;
    } else {
        list->last->next = number;
        list->last = number;
    }
}

void destroy_number_list(struct number_list * list) {
    struct number * aux;
    if (list == NULL) return;
    
    while (list->first) {
        aux = list->first; 
        list->first = list->first->next;
        free(aux);
    }

    free(list);
}

// ### helper debugging functions

void print_number_list(struct number_list * list) {
    struct number * number = list->first;

    printf("number list: ");
    while (number) {
        printf("%d (%d, %d) -> ", number->number, number->row, number->column); 
        number = number->next;
    }
    puts("NULL");
}

void print_symbol_list(struct symbol_list * list) {
    struct symbol * symbol = list->first;

    printf("symbol list: ");
    while (symbol) {
        printf("%c (%d, %d) -> ", symbol->symbol, symbol->row, symbol->column); 
        symbol = symbol->next;
    }
    puts("NULL");
}

// ### ACTUAL SOLUTION
int collide(struct number * number, struct symbol * symbol) {
    int nr, nc, nw, sr, sc;
    assert(number);
    assert(symbol);

    nr = number->row;
    nc = number->column;
    nw = number->width;
    sr = symbol->row;
    sc = symbol->column;

    return 
        nr >= (sr - 1) &&
        nr <= (sr + 1) &&
        ((nc >= (sc - 1) && nc <= (sc + 1)) ||
         ((nc + nw - 1) >= (sc - 1) && (nc + nw - 1) <= (sc + 1)) );
}

long long int calculate_part_number_sum(struct number_list * number_list, struct symbol_list * symbol_list) {
    long long result = 0;
    struct number * number;
    struct symbol * symbol;

    if (!number_list || !symbol_list) return 0;

    number = number_list->first;
    
    while (number) {
        symbol = symbol_list->first;

        while (symbol) {
            if (collide(number, symbol)) {
                result += number->number;
                break;
            }

            symbol = symbol->next;
        }

        number = number->next;
    }

    return result;
}

long long int calculate_gear_ratio_sum(struct symbol_list * symbol_list, struct number_list * number_list) {
    long long int result = 0, gear_ratio;
    int num_part_numbers;
    struct symbol * symbol;
    struct number * number;

    assert(symbol_list);
    assert(number_list);

    symbol = symbol_list->first;

    while (symbol) {
        if (symbol->symbol != '*') {
            symbol = symbol->next;
            continue;
        }

        num_part_numbers = 0;
        gear_ratio = 0;

        // probably there is a better way to do that. But right now I just want
        // to solve the problem :)

        number = number_list->first;

        // skip all numbers that are too far behind
        while (number && number->row < symbol->row - 1) {
            number = number->next;
        }

        while (number) {
            if (number->row > symbol->row + 1 ||
                (number->row == (symbol->row + 1) && number->column > (symbol->column + 1))) {
                // no need to look further, go for next symbol
                break;
            }

            if (collide(number, symbol)) {
                switch (num_part_numbers) {
                    case 0: {
                        gear_ratio = number->number; 
                    } break;
                    case 1: {
                        gear_ratio *= number->number; 
                    } break;
                }
                num_part_numbers++;
            }
            number = number->next;
        }

        if (num_part_numbers == 2) {
            result += gear_ratio;
        }

        symbol = symbol->next;
    }

    return result;
}

int main(int argc, char ** argv) {
    char line[MAX_LINE_SIZE + 1], *cursor, c;
    int reading_number, row, column;
    long long int result, result2;
    struct number current_number;
    struct symbol_list * symbol_list;
    struct number_list * number_list;
    FILE * input;
    
    if (argc < 2) {
        fprintf(stderr, "argument missing.\n");
    }

    input = fopen(argv[1], "r");
    if (!input) {
        fprintf(stderr, "Failed to open input file '%s'\n", argv[1]);
        return -1;
    }

    symbol_list = create_symbol_list();
    number_list = create_number_list();

    reading_number = 0;
    row = 0;
    column = 0;
    while(fgets(line, MAX_LINE_SIZE + 1, input) && !feof(input)) {
        cursor = line; 

        while (*cursor) {
            c = *cursor;

            if (c >= '0' && c <= '9') {
                if (!reading_number) {
                    reading_number        = 1;
                    current_number.row    = row;
                    current_number.column = column;
                    current_number.number = c - '0';
                    current_number.width  = 1;
                } else {
                    current_number.number *= 10;
                    current_number.number += c - '0';
                    current_number.width++;
                }
            } else if ((c == '.' || c == '\n')) {
                if (reading_number) {
                    reading_number = 0;
                    insert_number(
                        number_list,
                        create_number(
                            current_number.number,
                            current_number.row,
                            current_number.column,
                            current_number.width
                        )
                    );
                }
            } else { // it is a symbol
                if (reading_number) {
                    reading_number = 0;
                    insert_number(
                        number_list,
                        create_number(
                            current_number.number,
                            current_number.row,
                            current_number.column,
                            current_number.width
                        )
                    );
                }
                insert_symbol(symbol_list, create_symbol(c, row, column));
            }

            cursor++;
            column++;
        }

        column = 0;
        row++;
    }

    //print_number_list(number_list);
    //print_symbol_list(symbol_list);

    result  = calculate_part_number_sum(number_list, symbol_list);
    result2 = calculate_gear_ratio_sum(symbol_list, number_list);

    printf("part number sum: %lld\n", result);
    printf("gear ratio sum:  %lld\n", result2);

    destroy_symbol_list(symbol_list);
    destroy_number_list(number_list);

    fclose(input);

    return 0;
}
