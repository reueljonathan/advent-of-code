#ifndef RANGE_H
#define RANGE_H

struct range {
    unsigned long long int source;
    unsigned long long int destination;
    unsigned long long int length;
    struct range * next;
};

struct range_list {
    struct range * first;
    struct range * last;
};

struct range_list * create_range_list();
void insert_range(struct range_list * list, struct range * range);
void destroy_range_list(struct range_list * range_list);

struct range * create_range(unsigned long long int source, unsigned long long int destination, unsigned long long int length);

#endif
