#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <limits.h>
#include "range.h"
#include "category.h"

#define MAX_LINE_LENGTH 1024

struct number_array {
    unsigned long long int values[25];
    size_t size;
};

void print_seeds_list(struct number_array list) {
    printf("seeds:");
    for (size_t i = 0; i < list.size; i++) {
        printf(" %llu", list.values[i]); 
    }
    printf("\n\n");
}

void print_map(struct category_list * category_list) {
    struct category * category = category_list->first;
    struct range * range;

    while (category) {

        range = category->range_list->first;

        printf("a map:\n");

        while (range) {
            printf("%llu %llu %llu\n", range->destination, range->source, range->length);
            range = range->next;
        }

        category = category->next;
        printf("\n");
    }
}

unsigned long long int calculate_dest(struct category * category, unsigned long long int number) {
    unsigned long long int result = number; // If is not mapped, source == destination
    struct range * range;

    assert(category);
    assert(category->range_list);

    range = category->range_list->first;

    while (range) {

        // I am assuming there are no overlapping ranges
        if (number >= range->source && number <= (range->source + range->length - 1)) {
            result = range->destination + number - range->source;
            break;
        }

        range = range->next;
    }

    if (category->next) {
        return calculate_dest(category->next, result);
    } else {
        return result;
    }
}

int main(int argc, char ** argv) {
    FILE *input;
    char line[MAX_LINE_LENGTH + 1], *aux, *aux2;
    struct number_array seeds_list;
    int reading_category = 0;
    struct category_list * category_list;
    struct category * category;
    unsigned long long int destination, source, length, min_distance = ULONG_MAX, distance;

    if (argc < 2) {
        fprintf(stderr, "Input file missing.\n");
        return -1;
    }

    input = fopen(argv[1], "r");

    if (!input) {
        fprintf(stderr, "Failed to open file '%s'.\n", argv[1]);
        return -1;
    }

    category_list = create_category_list();

    seeds_list.size = 0;
    fgets(line, MAX_LINE_LENGTH + 1, input); // read seeds line
    strtok(line, ":"); // discard seeds part
    aux = strtok(NULL, ":"); // get seeds number list string

    aux2 = strtok(aux, " ");
    while (aux2) {
        seeds_list.values[seeds_list.size++] = strtoull(aux2, NULL, 10);

        //printf("seed found: %llu\n", seeds_list.values[seeds_list.size - 1]);

        aux2 = strtok(NULL, " ");
    }

    while (fgets(line, MAX_LINE_LENGTH + 1, input) && !feof(input)) {
        if (line[0] == '\n') {
            
            if (reading_category) {

                insert_category(category_list, category);
                reading_category = 0;
            }

        } else if (strstr(line, "map:")) {
            category = create_category();
            reading_category = 1;
        } else { 
            destination = strtoull(strtok(line, " "), NULL, 10); 
            source      = strtoull(strtok(NULL, " "), NULL, 10); 
            length      = strtoull(strtok(NULL, " "), NULL, 10); 

            //printf("%llu %llu %llu\n", destination, source, length);

            insert_range(category->range_list,
                create_range(source, destination, length));
        }
    }
    if (reading_category) {

        insert_category(category_list, category);
        reading_category = 0;
    }

    print_seeds_list(seeds_list);
    print_map(category_list);

    for (size_t i = 0; i < seeds_list.size; i++) {
        distance = calculate_dest(category_list->first, seeds_list.values[i]);
        min_distance = distance < min_distance ? distance : min_distance; 
    }

    printf("Min distance: %llu\n", min_distance);
     
    destroy_category_list(category_list);
    fclose(input);

    return 0;
}
