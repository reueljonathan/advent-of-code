// #include "range.h" <-- is this needed? (since I already include it in the
// header..
#include <stdlib.h>
#include <assert.h>
#include "category.h"

struct category_list * create_category_list() {
    struct category_list * list = malloc(sizeof(struct category_list));

    assert(list);

    list->first = list->last = NULL;

    return list;
}

void insert_category(struct category_list * list, struct category * category) {
    assert(list);

    if (list->first == NULL) {
        list->first = list->last = category;
    } else {
        list->last->next = category;
        list->last = category;
    }
}

void destroy_category_list(struct category_list * list) {
    struct category *category, *aux;
    assert(list);

    category = list->first;

    while (category) {
        aux = category;
        category = category->next;
        
        destroy_range_list(aux->range_list);
        free(aux);
    }

    free(list);
}

struct category * create_category() {
    struct category * category = malloc(sizeof(struct category));

    assert(category);

    category->range_list = create_range_list();
    category->next = NULL;

    return category;
}
