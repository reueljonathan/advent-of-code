#include <stdlib.h>
#include <assert.h>
#include "range.h"

struct range_list * create_range_list() {
    struct range_list * list = malloc(sizeof(struct range_list));

    assert(list);

    list->first = NULL;
    list->last  = NULL;

    return list;
}

void insert_range(struct range_list * list, struct range * range) {
    assert(list);

    if (list->first == NULL) {
        list->first = range;
        list->last  = range;
    } else {
        list->last->next = range;
        list->last       = range;
    }
}

void destroy_range_list(struct range_list * list) {
    struct range *range, *aux;

    if (!list) return;

    range = list->first;

    while (range) {
        aux = range;
        range = range->next;
        free(aux);
    }

    free(list);
}

struct range * create_range(unsigned long long int source, unsigned long long int destination, unsigned long long int length) {
    struct range * range = malloc(sizeof(struct range));

    assert(range);
    
    range->source      = source;
    range->destination = destination;
    range->length      = length;
    range->next        = NULL;

    return range;
}
