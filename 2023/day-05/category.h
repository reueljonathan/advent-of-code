#ifndef CATEGORY_H
#define CATEGORY_H

#include "range.h"

struct category {
    struct range_list * range_list;
    struct category * next;
};

struct category_list {
    struct category *first, *last;
};

struct category_list * create_category_list();
void insert_category(struct category_list * list, struct category * category);
void destroy_category_list(struct category_list * list);

struct category * create_category();

#endif
