#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#define MAX_LINE_LENGTH 1024

struct number_array {
    int    values[25];
    size_t size;
};

struct card {
    int    num_copies;
    int    *winning_numbers;
    size_t size_winning_numbers;
    int    *elf_numbers;
    size_t size_elf_numbers;
    struct card * next;
};

struct card * create_card(struct number_array winning, struct number_array elf) {
    struct card * result = malloc(sizeof(struct card));

    assert(result);

    result->num_copies = 1;

    result->winning_numbers = malloc(sizeof(int) * winning.size);
    assert(result->winning_numbers);
    result->size_winning_numbers = winning.size;

    memcpy(result->winning_numbers, winning.values, sizeof(int) * winning.size);

    result->elf_numbers = malloc(sizeof(int) * elf.size);
    assert(result->elf_numbers);
    result->size_elf_numbers = elf.size;

    memcpy(result->elf_numbers, elf.values, sizeof(int) * elf.size);

    result->next = NULL;

    return result;
}

void destroy_card(struct card * card) {
    assert(card);
    free(card->winning_numbers);
    free(card->elf_numbers);
    free(card);
}

struct card_list {
    struct card * first;
    struct card * last;
};

void print_card_list(struct card_list * list);

struct card_list * create_list() {
    struct card_list *result = malloc(sizeof(struct card_list));

    assert(result);

    result->first = NULL;
    result->last  = NULL;

    return result;
}

void insert_card(struct card_list * list, struct card * card) {
    assert(list);

    if (list->first == NULL) {
        list->first = card;
        list->last  = card;
    } else {
        list->last->next = card;
        list->last = card;
    }
}

void destroy_list(struct card_list * list) {
    struct card *card, *aux;

    assert(list);

    card = list->first;
    while (card) {
        aux = card;
        card = card->next;
        destroy_card(aux);
    }

    free(list);
}

long long int calculate_total_scratchcards(struct card_list *list) {
    long long int result = 0;
    int num_winning_numbers;
    struct card *card, *aux;

    assert(list);

    card = list->first;
    while (card) {
        num_winning_numbers = 0;
        print_card_list(list);

        for (size_t i = 0; i < card->size_winning_numbers; i++) {
            for (size_t j = 0; j < card->size_elf_numbers; j++) {
                if (card->winning_numbers[i] == card->elf_numbers[j]) {
                    num_winning_numbers++;
                }
            }
        }

        aux = card->next;

        printf("num_winning_numbers: %d\n", num_winning_numbers);

        for (int i = 0; aux && i < num_winning_numbers; i++) {
            aux->num_copies += card->num_copies;
            aux = aux->next;
        }
        
        result += card->num_copies;
        card = card->next;
    }

    return result;
}

void print_card_list(struct card_list * list) {
    struct card * card = list->first;

    while(card) {
        printf("{ num_copies: %d } -> ", card->num_copies);
        card = card->next;
    }
    puts("NULL");
}

int main(int argc, char ** argv) {
    FILE *input;
    char line[MAX_LINE_LENGTH + 1], *cursor, *aux;
    char winning_list[MAX_LINE_LENGTH + 1], elf_list[MAX_LINE_LENGTH + 1];
    struct number_array winning_numbers, elf_numbers;
    struct card_list *card_list;

    if (argc < 2) {
        fprintf(stderr, "Input file missing.\n");
        return -1;
    }

    input = fopen(argv[1], "r");

    if (!input) {
        fprintf(stderr, "Failed to open file '%s'.\n", argv[1]);
        return -1;
    }

    card_list = create_list();

    while (fgets(line, MAX_LINE_LENGTH + 1, input) && !feof(input)) {
        // skip "Card XX:" part
        cursor = index(line, ':');
        cursor++;

        aux = strtok(cursor, "|");
        strncpy(winning_list, aux, MAX_LINE_LENGTH);
        aux = strtok(NULL, "|");
        strncpy(elf_list, aux, MAX_LINE_LENGTH);

        //printf("winning list: %s, elf list: %s\n", winning_list, elf_list);

        winning_numbers.size = 0;
        elf_numbers.size = 0;
    
        // parse winning numbers
        aux = strtok(winning_list, " ");
        while (aux) {
            winning_numbers.values[winning_numbers.size++] = atoi(aux);
            aux = strtok(NULL, " ");
        }

        // parse elf numbers
        aux = strtok(elf_list, " ");
        while (aux) {
            elf_numbers.values[elf_numbers.size++] = atoi(aux);
            aux = strtok(NULL, " ");
        }

        insert_card(card_list, create_card(winning_numbers, elf_numbers));
    }

    printf("num of scratchcards: %lld\n", calculate_total_scratchcards(card_list));
    destroy_list(card_list);
    fclose(input);

    return 0;
}
