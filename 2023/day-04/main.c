#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#define MAX_LINE_LENGTH 1024

struct number_array {
    int    values[25];
    size_t size;
};

int main(int argc, char ** argv) {
    FILE *input;
    char line[MAX_LINE_LENGTH + 1], *cursor, *aux;
    char winning_list[MAX_LINE_LENGTH + 1], elf_list[MAX_LINE_LENGTH + 1];
    long long int points_sum = 0, line_points;
    struct number_array winning_numbers, elf_numbers;

    if (argc < 2) {
        fprintf(stderr, "Input file missing.\n");
        return -1;
    }

    input = fopen(argv[1], "r");

    if (!input) {
        fprintf(stderr, "Failed to open file '%s'.\n", argv[1]);
        return -1;
    }

    while (fgets(line, MAX_LINE_LENGTH + 1, input) && !feof(input)) {
        line_points = 0;
        // skip "Card XX:" part
        cursor = index(line, ':');
        cursor++;

        aux = strtok(cursor, "|");
        strncpy(winning_list, aux, MAX_LINE_LENGTH);
        aux = strtok(NULL, "|");
        strncpy(elf_list, aux, MAX_LINE_LENGTH);

        //printf("winning list: %s, elf list: %s\n", winning_list, elf_list);

        winning_numbers.size = 0;
        elf_numbers.size = 0;
    
        // parse winning numbers
        aux = strtok(winning_list, " ");
        while (aux) {
            winning_numbers.values[winning_numbers.size++] = atoi(aux);
            aux = strtok(NULL, " ");
        }

        // parse elf numbers
        aux = strtok(elf_list, " ");
        while (aux) {
            elf_numbers.values[elf_numbers.size++] = atoi(aux);
            aux = strtok(NULL, " ");
        }

        for (size_t i = 0; i < elf_numbers.size; i++) {
            for (size_t j = 0; j < winning_numbers.size; j++) {
                if (elf_numbers.values[i] == winning_numbers.values[j]) {
                    if (line_points) {
                        line_points *= 2;
                    } else {
                        line_points = 1;
                    }
                }
            }
        }

        points_sum += line_points;
    }

    printf("sum of points: %lld\n", points_sum);

    fclose(input);

    return 0;
}
